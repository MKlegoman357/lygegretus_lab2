package data

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Value struct {
	Name  string  `json:"name"`
	Count int     `json:"count"`
	Value float32 `json:"value"`
	Total float32 `json:"-"`
}

func (value *Value) String() string {
	return fmt.Sprintf("%s (%d * %f = %f)", value.Name, value.Count, value.Value, value.Total)
}

func (value *Value) CompareTo(other *Value) int {
	var diff = value.Total - other.Total

	if diff < 0 {
		return -1
	} else if diff > 0 {
		return 1
	} else {
		return 0
	}
}

type ValueSlice []*Value

func (items ValueSlice) PrintFirstFew(max int) {
	for i := 0; i < max && i < len(items); i++ {
		fmt.Printf("%d. %s\n", i, items[i])
	}
}

func (items ValueSlice) Print() {
	for i, item := range items {
		fmt.Printf("%d. %s\n", i, item)
	}
}

func WriteValuesToFile(values []*Value, path string) (err error) {
	bytes, err := json.MarshalIndent(values, "", "  ")

	if err != nil {
		return err
	}

	err = ioutil.WriteFile(path, bytes, 0644)

	if err != nil {
		return err
	}

	return nil
}

func ReadValuesFromFile(path string) (values []*Value, err error) {
	bytes, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, &values)

	if err != nil {
		return nil, err
	}

	return
}

var Names = []string{
	"measure",
	"balance",
	"crack",
	"false",
	"right",
	"smoke",
	"walk",
	"scream",
	"town",
	"afraid",
	"brief",
	"lame",
	"icy",
	"long-term",
	"trashy",
	"lovely",
	"sniff",
	"own",
	"tangy",
	"arrange",
	"decide",
	"ticket",
	"untidy",
	"moaning",
	"swanky",
	"stroke",
	"chilly",
	"oval",
	"warn",
	"foot",
	"tart",
	"planes",
	"salty",
	"destruction",
	"pointless",
	"hope",
	"file",
	"pot",
	"clumsy",
	"pumped",
	"murder",
	"quilt",
	"sky",
	"fine",
	"paddle",
	"telephone",
	"marked",
	"record",
	"level",
	"unsightly",
	"hanging",
	"jar",
	"squeeze",
	"live",
	"aware",
	"interfere",
	"queen",
	"way",
	"honey",
	"skate",
	"encourage",
	"frightened",
	"callous",
	"gaze",
	"geese",
	"remarkable",
	"girls",
	"intend",
	"cake",
	"girl",
	"bruise",
	"zippy",
	"glossy",
	"moon",
	"adhesive",
	"ashamed",
	"harsh",
	"gifted",
	"obese",
	"futuristic",
	"mint",
	"quicksand",
	"equable",
	"charge",
	"eatable",
	"aboriginal",
	"receipt",
	"inform",
	"changeable",
	"trot",
	"friend",
	"romantic",
	"jumpy",
	"driving",
	"song",
	"press",
	"respect",
	"reflect",
	"elite",
	"fancy",
}
