package main

import (
	"./data"
	"./utils"
	"bufio"
	"fmt"
	"io"
	"math"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	if len(os.Args) == 2 && (os.Args[1] == "--generate") {
		generateData()
	} else {
		createFilteredData()
	}
}

func generateData() {
	var reader = bufio.NewReader(os.Stdin)

	var valueCount = getInt(
		reader,
		"Enter amount of values to generate (must be a positive integer):",
		"Invalid amount!",
		100,
		func(value int) bool {
			return value > 0
		},
	)

	var totalMin, totalMax = getFloatRange(
		reader,
		"Enter desired range of resulting total (ex.: '<100', '>10', '10 100'):",
		"<100",
		-math.MaxFloat32,
		math.MaxFloat32,
	)

	var values = generateValues(valueCount, data.Names, totalMin, totalMax)

	if getBool(reader, "Print values?", false) {
		fmt.Println("Generated values:")
		data.ValueSlice(values).Print()
		fmt.Println()
	}

	fmt.Print("Save to file (enter empty path to not save): ")
	outputPath, _ := getLine(reader)

	if len(outputPath) > 0 {
		if !strings.HasSuffix(outputPath, ".json") {
			outputPath += ".json"
		}

		err := data.WriteValuesToFile(values, outputPath)

		if err != nil {
			fmt.Printf("An error occured while trying to save the file: \n%s\n", err.Error())
			return
		}

		fmt.Printf("Values saved to file successfully to %s.\n", outputPath)
	} else {
		fmt.Println("Skipped saving to file.")
	}
}

func createFilteredData() {
	var reader = bufio.NewReader(os.Stdin)

	fmt.Print("Input file: ")

	inputFilePath, _ := getLine(reader)
	stat, err := os.Stat(inputFilePath)

	if os.IsNotExist(err) {
		fmt.Printf("File '%s' does not exist.\n", inputFilePath)
		return
	} else if stat.IsDir() {
		fmt.Printf("File '%s' is a directory.\n", inputFilePath)
		return
	}

	values, err := data.ReadValuesFromFile(inputFilePath)

	if err != nil {
		fmt.Printf("Error while trying to read the file:\n%s\n", err.Error())
	}

	var processorCount = runtime.NumCPU()
	var workerCount = getInt(
		reader,
		"Enter worker goroutine count:",
		"Invalid amount!",
		processorCount-1,
		func(value int) bool {
			return value > 0
		},
	)

	var bufferSize = getInt(
		reader,
		"Enter input queue buffer size:",
		"Invalid amount!",
		(len(values)+1)/2,
		func(value int) bool {
			return value > 0
		},
	)

	var filterFrom, filterTo = getFloatRange(
		reader,
		"Filter values with total (ex.: '<100', '>10', '10 100'):",
		"<50",
		-math.MaxFloat32,
		math.MaxFloat32,
	)

	var results []*data.Value

	if getBool(reader, "Show console output?", false) {
		results = consumeAndFilterValuesWithConsoleOutput(values, filterFrom, filterTo, workerCount, bufferSize)
	} else {
		results = consumeAndFilterValues(values, filterFrom, filterTo, workerCount, bufferSize)
	}

	fmt.Print("Result file path (leave empty to output to the console): ")
	outputFilePath, _ := getLine(reader)

	if len(outputFilePath) > 0 {
		if !strings.HasSuffix(outputFilePath, ".txt") {
			outputFilePath += ".txt"
		}

		file, err := os.Create(outputFilePath)

		if err != nil {
			fmt.Printf("An error occurred while trying to open the output file:\n%s\n", err.Error())
			return
		}

		_, _ = fmt.Fprintln(file, "Original values:")
		printValuesAsTable(file, values)

		_, _ = fmt.Fprintln(file)
		_, _ = fmt.Fprintln(file, "Filtered values:")
		printValuesAsTable(file, results)

		_ = file.Close()
	} else {
		fmt.Println()
		fmt.Println("Filtered results:")
		printValuesAsTable(os.Stdout, results)
	}
}

func consumeAndFilterValues(values []*data.Value, totalFrom, totalTo float32, workerCount, bufferSize int) []*data.Value {
	var inputChannel = make(chan *data.Value)
	var dataChannel = make(chan *data.Value)

	var resultChannel = make(chan *data.Value)
	var outputChannel = make(chan *data.Value)

	// launch goroutines
	for i := 0; i < workerCount; i++ {
		// worker goroutine
		go func() {
			for value := range dataChannel {
				value.Total = float32(value.Count) * value.Value

				if value.Total >= totalFrom && value.Total <= totalTo {
					resultChannel <- value
				}
			}

			resultChannel <- nil
		}()
	}

	// data goroutine
	go func() {
		doBufferedDataTransfer(inputChannel, dataChannel, bufferSize, -1)
	}()

	// result goroutine
	go func() {
		doBufferedDataTransfer(resultChannel, outputChannel, len(values), workerCount)
	}()

	// main

	for _, value := range values {
		inputChannel <- value
	}

	close(inputChannel)

	var output = make([]*data.Value, 0, len(values))

	for value := range outputChannel {
		var index = len(output)
		output = output[:index+1]

		for index > 0 && output[index-1].CompareTo(value) > 0 {
			output[index] = output[index-1]
			index--
		}

		output[index] = value
	}

	return output
}

func doBufferedDataTransfer(input <-chan *data.Value, output chan<- *data.Value, bufferSize int, closeAfterNils int) {
	var buffer = make([]*data.Value, 0, bufferSize)
	var inputOpen = true

	var processInput = func(value *data.Value, stillOpen bool) {
		if !stillOpen {
			inputOpen = false
			return
		} else if value == nil {
			closeAfterNils--
		} else {
			buffer = append(buffer, value)
		}
	}

	var processOutput = func() {
		buffer = buffer[:len(buffer)-1]
	}

	for inputOpen && closeAfterNils != 0 || len(buffer) > 0 {
		if len(buffer) <= 0 {
			value, stillOpen := <-input
			processInput(value, stillOpen)
		} else if len(buffer) >= cap(buffer) {
			output <- buffer[len(buffer)-1]
			processOutput()
		} else {
			select {
			case value, stillOpen := <-input:
				processInput(value, stillOpen)
			case output <- buffer[len(buffer)-1]:
				processOutput()
			}
		}
	}

	close(output)
}

func consumeAndFilterValuesWithConsoleOutput(values []*data.Value, totalFrom, totalTo float32, workerCount, bufferSize int) []*data.Value {
	var waitGroup sync.WaitGroup
	var workerWaitGroup sync.WaitGroup

	var inputChannel = make(chan *data.Value)
	var dataChannel = make(chan *data.Value)

	var resultChannel = make(chan *data.Value)
	var outputChannel = make(chan *data.Value)

	workerWaitGroup.Add(workerCount)

	// launch goroutines
	for i := 0; i < workerCount+3; i++ {
		var index = i - 3
		waitGroup.Add(1)

		if i == 0 {
			// data goroutine
			go func() {
				defer waitGroup.Done()
				doBufferedDataTransferWithConsoleOutput(inputChannel, dataChannel, bufferSize, "DATA")
			}()
		} else if i == 1 {
			// result goroutine
			go func() {
				defer waitGroup.Done()
				doBufferedDataTransferWithConsoleOutput(resultChannel, outputChannel, len(values), "RESULT")
			}()
		} else if i == 2 {
			// closes the result channel after all workers are done
			go func() {
				defer waitGroup.Done()
				workerWaitGroup.Wait()
				fmt.Printf("[CLOSE RESULT] Closing result channel\n")
				close(resultChannel)
			}()
		} else {
			// worker goroutine
			go func() {
				defer waitGroup.Done()
				defer workerWaitGroup.Done()

				for {
					fmt.Printf("[WORKER %d] Waiting for value\n", index)
					var value, stillOpen = <-dataChannel
					fmt.Printf("[WORKER %d] Got value: %s %t \n", index, value, stillOpen)

					if !stillOpen {
						fmt.Printf("[WORKER %d] Exiting worker\n", index)
						return
					}

					value.Total = float32(value.Count) * value.Value

					if value.Total >= totalFrom && value.Total <= totalTo {
						fmt.Printf("[WORKER %d] Putting value to results: %s\n", index, value)
						resultChannel <- value
						fmt.Printf("[WORKER %d] Value put to results: %s\n", index, value)
					} else {
						fmt.Printf("[WORKER %d] Ignoring value: %s\n", index, value)
					}
				}
			}()
		}
	}

	// main

	for _, value := range values {
		fmt.Printf("[MAIN] Putting value: %s\n", value)
		inputChannel <- value
		fmt.Printf("[MAIN] Value has been put: %s\n", value)
	}

	fmt.Printf("[MAIN] Closing input channel\n")
	close(inputChannel)

	var output = make([]*data.Value, 0, len(values))

	for value := range outputChannel {
		fmt.Printf("[MAIN] Got result value %s\n", value)
		var index = len(output)
		output = output[:index+1]

		for index > 0 && output[index-1].CompareTo(value) > 0 {
			output[index] = output[index-1]
			index--
		}

		output[index] = value
	}

	waitGroup.Wait()

	return output
}

func doBufferedDataTransferWithConsoleOutput(input <-chan *data.Value, output chan<- *data.Value, bufferSize int, name string) {
	var buffer = make([]*data.Value, 0, bufferSize)
	var inputOpen = true

	var processInput = func(value *data.Value, stillOpen bool) {
		fmt.Printf("[%s BUFFER] Got value: %s %t\n", name, value, stillOpen)

		if !stillOpen {
			fmt.Printf("[%s BUFFER] Input closed\n", name)
			inputOpen = false
			return
		}

		buffer = append(buffer, value)
	}

	var processOutput = func() {
		fmt.Printf("[%s BUFFER] Value was taken from buffer: %s\n", name, buffer[len(buffer)-1])
		buffer = buffer[:len(buffer)-1]
	}

	for {
		if len(buffer) <= 0 {
			fmt.Printf("[%s BUFFER] Waiting for value\n", name)
			value, stillOpen := <-input
			processInput(value, stillOpen)
		} else if len(buffer) >= cap(buffer) {
			fmt.Printf("[%s BUFFER] Waiting for output: %s\n", name, buffer[len(buffer)-1])
			output <- buffer[len(buffer)-1]
			processOutput()
		} else {
			fmt.Printf("[%s BUFFER] Waiting for input or output (%s)\n", name, buffer[len(buffer)-1])
			select {
			case value, stillOpen := <-input:
				processInput(value, stillOpen)
			case output <- buffer[len(buffer)-1]:
				processOutput()
			}
		}

		if !inputOpen && len(buffer) <= 0 {
			fmt.Printf("[%s BUFFER] Closing output\n", name)
			close(output)
			return
		}
	}
}

func generateValues(amount int, names []string, totalMin, totalMax float32) []*data.Value {
	values := make([]*data.Value, amount)

	for i := range values {
		var total = totalMin + (totalMax-totalMin)*rand.Float32()
		var count = rand.Intn(10)
		var value float32

		if count == 0 && (totalMin > 0 || totalMax < 0) {
			count = 1
		}

		if count == 0 {
			value = total
		} else {
			value = total / float32(count)
		}

		values[i] = &data.Value{
			Name:  names[rand.Intn(len(names))],
			Count: count,
			Value: value,
			Total: float32(count) * value,
		}
	}

	return values
}

func getLine(reader *bufio.Reader) (line string, err error) {
	line, err = reader.ReadString('\n')

	if err == nil {
		line = line[:len(line)-1]
	}

	return
}

func getInt(reader *bufio.Reader, message, wrongFormatMessage string, defaultValue int, cond func(value int) bool) (value int) {
	for {
		fmt.Printf("%s (%d) ", message, defaultValue)
		stringValue, _ := getLine(reader)

		if len(stringValue) == 0 {
			value = defaultValue
		} else {
			intValue, err := strconv.ParseInt(stringValue, 10, 0)

			if err == nil {
				value = int(intValue)
			} else {
				fmt.Printf("%s %s\n", wrongFormatMessage, err.Error())
			}
		}

		if cond(value) {
			return
		}
	}
}

func getFloatRange(reader *bufio.Reader, message, defaultValue string, clampFrom, clampTo float32) (from, to float32) {
	from, to = clampTo, clampFrom

	for from > to {
		fmt.Printf("%s (%s) ", message, defaultValue)
		var rangeString, _ = getLine(reader)
		var err error = nil

		if len(rangeString) == 0 {
			rangeString = defaultValue
		}

		if rangeString[0] == '<' {
			_, err = fmt.Fscanf(strings.NewReader(rangeString[1:]), "%f", &to)

			if err == nil {
				from = clampFrom
			}
		} else if rangeString[0] == '>' {
			_, err = fmt.Fscanf(strings.NewReader(rangeString[1:]), "%f", &from)

			if err == nil {
				to = clampTo
			}
		} else {
			_, _ = fmt.Fscanf(strings.NewReader(rangeString), "%f %f", &from, &to)
		}

		if from < clampFrom {
			from = clampFrom
		}

		if to > clampTo {
			to = clampTo
		}

		if from > to {
			if err != nil {
				fmt.Printf("Invalid range! %s\n", err.Error())
			} else {
				fmt.Println("Invalid range!")
			}
		}
	}

	return
}

func getBool(reader *bufio.Reader, message string, defaultValue bool) (value bool) {
	var defaultString = "y/N"

	if defaultValue {
		defaultString = "Y/n"
	}

	fmt.Printf("%s (%s) ", message, defaultString)
	stringValue, _ := getLine(reader)

	if len(stringValue) == 0 {
		return defaultValue
	}

	return stringValue[0] == 'y'
}

func printValuesAsTable(writer io.Writer, values []*data.Value) {
	var cells = make([][]string, len(values))

	for i, value := range values {
		cells[i] = []string{
			value.Name,
			strconv.Itoa(value.Count),
			fmt.Sprintf("%f", value.Value),
			fmt.Sprintf("%f", value.Total),
		}
	}

	utils.PrintTable(writer, &[]string{"Name", "Count", "Value", "Total"}, &cells)
}
